# SaberMod Prebuilt Libraries #

## To use these toolchains you must first install some prebuilt libs into your system.##
So take sabermod-prebuilt-DATE.zip and unzip it into your lib path.

**For Debian/Ubuntu/Mint**

* Unzip the lib files to /usr/lib/x86_64-linux-gnu/

* Unzip the include files to /usr/include/x86_64-linux-gnu/


**For Arch Linux**

* Unzip the lib files into /usr/lib

* Unzip the include files to /usr/include/


There! Now you're ready to build using SaberMod.